const publicPath = "/";
const privatePath = "/admin";

const routeCodes = {
    HOMEPAGE: publicPath,
    WORK: `${publicPath}work`,
    ABOUT_US: `${publicPath}about`,
    CONTACT: `${publicPath}contact`,
    Blog: `${publicPath}blog`,
    Post: `${publicPath}blog/:id`,
    ILLUSTRATION_DETAILS: `${publicPath}work/illustration/:id`,
    VIDEO_DETAILS: `${publicPath}work/video/:id`
}

export default routeCodes;
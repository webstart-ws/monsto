import React from 'react';
import { Link } from 'react-router-dom';
import "./Button.scss";

export const Button = ({
  href = '#',
  color = '#FD4B61',
  children
}) => {
  return (
    <Link className="mst-button" to={href} style={{
      background: color
    }}>
      {children}
    </Link>
  );
};
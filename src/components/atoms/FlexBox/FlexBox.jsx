import React, { forwardRef } from 'react';

export const FlexBox = forwardRef((
  {
    type = 'div',
    className = '',
    id = '',
    children,
    style = {},
    onClick = () => { },
    ...rest
  },
  ref,
) =>
  (
    React.createElement(
      type,
      {
        id: id,
        ref,
        className: `mst-flexbox d-flex ${className}`,
        style,
        onClick,
        ...rest
      },
      children,
    )
  ),
);

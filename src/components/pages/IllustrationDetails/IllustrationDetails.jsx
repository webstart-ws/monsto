import React, { useEffect, useState } from "react";
import { scrollToTop } from "utils";
import { NeedAnimation } from "components/organisms/NeedAnimation/NeedAnimation";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import "./IllustrationDetails.scss";
import axios from "axios";
const apiUrl = process.env.REACT_APP_API;
const IllustrationDetails = ({ match }) => {
  let { id: illustartion } = match.params;
  const [state, setState] = useState({});
  useEffect(() => {
    getIlustrationItem().then((res) => {
      let item = {};
      res.map((resItem) => {
        if (resItem.id == illustartion) {
          item = resItem;
        }
      });
      if (item) {
        setState(item);
      }
    });
    scrollToTop();
  }, []);
  const getIlustrationItem = async () => {
    let queryParams = {};
    queryParams.category = 3;
    queryParams.api_key = 111;
    let res = await axios.get(`${apiUrl}/api/getportfolio`, {
      params: queryParams,
    });
    let { data } = res;
    return data;
  };
  let x = state;
  return (
    <div className="mst-illustration-details">
      <FlexBox
        type="section"
        className="max1100 flex-column justify-content-center align-items-center mst-illustration-details__inner"
      >
        <h1>{x.name}</h1>
        {
          <React.Fragment>
            {/*<span className="mst-illustration-details__about">{x.description}</span>*/}
            <p className="mst-illustration-details__description"  dangerouslySetInnerHTML={{
              __html: x.description,
            }}>
            </p>
          </React.Fragment>
        }
        <div className="w-100 content">
          {x.show &&
            x.show.map((image,index) => (
              <img
                key={index}
                role="presentation"
                className="mst-illustration-details__image"
                src={`${apiUrl}/portfolio/` + image}
                alt=""
              />
            ))}
        </div>
      </FlexBox>
      <NeedAnimation />
    </div>
  );
};

export default IllustrationDetails;

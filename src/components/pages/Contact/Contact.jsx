import React, { useEffect, useState } from 'react';
import { Helmet } from "react-helmet";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import { scrollToTop } from "utils";
import { projectTypes, budget, findUsTypes } from "./settings";
import Dragon2 from "assets/images/Dragon2.jpg";
import "./Contact.scss";
import axios from "axios";

const apiUrl = process.env.REACT_APP_API;


const Contact = () => {

    
    const [requestSendWithSuccess, setSuccess] = useState(false);
    const [requestError, setRequestError] = useState(false);
    
    useEffect(()=>{
        scrollToTop(); 
    },[])

    const [data, setData ] = useState({
        projectType: projectTypes[0].title,
        budget: budget[0].title,
        description: '',
        contacts: {
            name: '',
            email: '',
            company: '',
            phone: ''
        },
        howFindUs: findUsTypes[0].title,
    })

    const [errors, setErrors] = useState({
        projectType: false,
        budget: false,
        description: false,
        contacts: {
            name: false,
            email: false,
            company: false,
            phone: false          
        },
        howFindUs: false
    });


    const handleTypeChange = (key,title) => {

        if (errors[key]) {
            setErrors(prev => {
                return {
                    ...prev,
                    [key]: false
                }
            })
        }

        const newData = {
            ...data,
            [key] : title
        }
        setData(newData);
    }

    const handleInputChange = (key,title) => {

        if (errors.contacts[key]) {
            setErrors(prev => {
                return {
                    ...prev,
                    contacts : {
                        ...prev.contacts,
                        [key]: false
                    }
                }
            })
        }

        const newData = {
            ...data,
            contacts: {
                ...data.contacts,   
                [key]: title
            }
        }
        setData(newData);
    }

    const handleSubmit = () => {
        setRequestError(false);
        const hasErrors = validate();
        if (hasErrors) return;

        sendEmail();

    }

    const sendEmail = () => {
        let dataEncoded = JSON.stringify(data)
        axios.post(`${apiUrl}/api/formmail`,{
            data:dataEncoded,
            api_key: 111,
        }).then(
            function (response) {
                setSuccess(true);
                console.log(data);
            },
            function (err) {
                setRequestError(true);

            }
        );
    }


    const validate = () => {
        let hasError = false;
        Object.keys(data).map((val) => {
            if (!data[val]) {
                hasError = true;
                setErrors(prev => {
                    return {
                        ...prev,
                        [val]: true
                    }
                })
            }
        })

        if(!data.contacts.name) {
            setErrors(prev => {
                return {
                    ...prev,
                    contacts : {
                        ...prev.contacts,
                        name: true
                    }
                }
            });
            hasError = true;
        }

        if(!data.contacts.email) {
            setErrors(prev => {
                return {
                    ...prev,
                    contacts : {
                        ...prev.contacts,
                        email: true
                    }
                }
            });
            hasError = true;
        }

        if(!data.contacts.company) {
            setErrors(prev => {
                return {
                    ...prev,
                    contacts : {
                        ...prev.contacts,
                        company: true
                    }
                }
            });
            hasError = true;
        }

        return hasError;
    }


    return (
        <div className="mst-contact">
            <Helmet>
                <title>Monsto - Contact</title>
                <meta
                    property="og:image"
                    content="https://i.imgur.com/3AcsSci.png"
                />
            </Helmet>
            <FlexBox type="section" className="max1100 mst-contact__inner flex-column justify-content-center">
                <FlexBox className="mst-contact__header justify-content-between align-items-center">
                    <div className="mst-contact__header-inner">
                        <h1 className="mst-contact__title">Fill the brief!</h1>
                        {/*<p className="mst-contact__number">Or call us <a href="tel:+37477497966">+37477497966</a> to fill together</p>*/}
                    </div>
                    <div className="mst-contact__logo-container">
                        <img src={Dragon2} alt=""/>
                    </div>
                </FlexBox>
                {
                    requestSendWithSuccess && (
                        <div className="success-content">
                            <h1>Thank you for contacting us!</h1>
                            <h6>We will be in touch soon</h6>
                        </div>
                    )
                }
                {
                    !requestSendWithSuccess && (
                     <FlexBox className="flex-column">

                    {/* TYPE OF PROJECT */}
                    <div className="mst-contact__group">
                        <div className="mst-contact__settings-title">Type of project</div>
                       <div className="mst-contact__settings-container">
                            {
                                projectTypes.map((type) => (
                                    <div
                                        key={type.id}
                                        role="presentation"
                                        onClick={() => handleTypeChange('projectType',type.title)}
                                        className={`mst-contact__settings-item ${type.title === data.projectType ? 'active' : 'inactive'}`}>{type.title}</div>
                                ))
                            }
                       </div>
                    </div>

                    {/* BUDGET */}
                    <div className="mst-contact__group">
                        <div className="mst-contact__settings-title">Budget</div>
                       <div className="mst-contact__settings-container">
                            {
                                budget.map((type) => (
                                    <div
                                        key={type.id}
                                        role="presentation"
                                        onClick={() => handleTypeChange('budget',type.title)}
                                        className={`mst-contact__settings-item ${type.title === data.budget ? 'active' : 'inactive'}`}>{type.title}</div>
                                ))
                            }
                       </div>
                    </div>

                    {/* DESCRIPTION */}
                    <div className="mst-contact__group">
                        <div className="mst-contact__settings-title">Your project description here</div>
                        <div className="mst-contact__description-container">
                            {
                                errors.description && (
                                    <span className="contact-error-label">Fields is required</span>
                                )
                            }
                           <textarea 
                                name="description"
                                className={`mst-contact__description ${errors.description ? 'error' : ''}`}
                                value={data.description}
                                onChange={(e) => handleTypeChange("description", e.target.value)}
                                placeholder="Type" name="" id="" cols="30" rows="10">
                            </textarea>
                       </div>
                    </div>

                    {/* CONTACTS */}
                    <div className="mst-contact__group">
                        <div className="mst-contact__settings-title">Your contacts</div>
                        <div className="mst-contact__contacts-container">
                            <div>
                                <input
                                    className={`${errors.contacts.name ? 'error' : ''}`}
                                    type="text"
                                    name="name"
                                    placeholder="Name"
                                    value={data.contacts.name}
                                    onChange={(e) => handleInputChange(e.target.name, e.target.value)}
                                />
                                {
                                    errors.contacts.name && (
                                        <span className="contact-error-label">Fields is required</span>
                                    )
                                }
                            </div>
                            <div>
                                <input
                                    className={`${errors.contacts.email ? 'error' : ''}`}
                                    type="email"
                                    name="email"
                                    placeholder="Email"
                                    value={data.contacts.email}
                                    onChange={(e) => handleInputChange(e.target.name, e.target.value)}
                                />
                                {
                                    errors.contacts.email && (
                                        <span className="contact-error-label">Fields is required</span>
                                    )
                                }
                            </div>
                            <div>
                                <input
                                    className={`${errors.contacts.company ? 'error' : ''}`}
                                    type="company"
                                    name="company"
                                    placeholder="Company"
                                    value={data.contacts.company}
                                    onChange={(e) => handleInputChange(e.target.name, e.target.value)}
                                />
                                {
                                    errors.contacts.company && (
                                        <span className="contact-error-label">Fields is required</span>
                                    )
                                }
                            </div>
                            <div>
                                <input
                                    className={`${errors.contacts.phone ? 'error' : ''}`}
                                    type="phone"
                                    name="phone"
                                    placeholder="Phone Number"
                                    value={data.contacts.phone}
                                    onChange={(e) => handleInputChange(e.target.name, e.target.value)}
                                />
                            </div>
                       </div>
                    </div>

                    {/* HOW YOU FIND US */}
                    <div className="mst-contact__group">
                        <div className="mst-contact__settings-title">How you find us?</div>
                        <div className="mst-contact__find-us-container">
                            {
                                findUsTypes.map((item,index)=>(
                                   <div key={index.toString()} className="mst-contact__find-us__item">
                                        <input
                                            id={`howFindUs${item.id}`}
                                            className="howFindUs"
                                            name="howFindUs"
                                            checked = {item.title === data.howFindUs}
                                            onChange={(e) => handleTypeChange("howFindUs", item.title)}
                                            type="checkbox" />
                                        <label htmlFor={`howFindUs${item.id}`}>{item.title}</label>
                                   </div>
                                ))
                            }
                        </div>
                    </div>
                
                    <div className="mst-contact__submit-container">
                        <button onClick={handleSubmit}>
                            Send
                        </button>
                    </div>
                    {
                        requestError && (
                            <div className="request-error">
                                <span>Oops something went wrong...</span>
                            </div>
                        )
                    }
                    <div className="mst-contact__team-contact-information">
                        <a href="mailto:team@monsto.net">team@monsto.net</a>
                    </div>
                </FlexBox>
                    )
                }        
         
            </FlexBox>
        </div>
    );
}

export default Contact;

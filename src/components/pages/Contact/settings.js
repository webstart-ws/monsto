export const projectTypes = [
  {
    id: 1,
    title: 'Logo Animation',
    value: 'logo-animation'
  },
  {
    id: 2,
    title: 'Video Expleiner',
    value: 'video-expleiner'
  },
  {
    id: 3,
    title: 'Character Creation',
    value: 'character-creation'
  },
  {
    id: 4,
    title: 'Loader/Icon design',
    value: 'loader-icon-design'
  },
  {
    id: 5,
    title: 'Character Animation',
    value: 'character-animation'
  },
  {
    id: 6,
    title: 'Logo Design',
    value: 'logo-design'
  },
  {
    id: 7,
    title: 'Concept illustrations',
    value: 'concept-illustrations'
  },
  {
    id: 8,
    title: 'Loader/Icon Animation',
    value: 'loader-icon-animation'
  },
];

export const budget = [
  {
    id: 1,
    title: 'under $500',
    value: 'under-500'
  },
  {
    id: 2,
    title: '$ 500 - $ 2000',
    value: '500-200'
  },
  {
    id: 3,
    title: '$ 2000 - $ 5000',
    value: '2000-5000'
  },
  {
    id: 4,
    title: 'Don\'t know',
    value: 'dont-know'
  }
];

export const findUsTypes = [
  {
    id: 1,
    title: 'Social Media',
    value: 'smm'
  },
  {
    id: 2,
    title: 'Google',
    value: 'google'
  },
  {
    id: 3,
    title: 'Other',
    value: 'other'
  }
];
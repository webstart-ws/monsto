import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import { scrollToTop } from "utils";
import { Link } from "react-router-dom";
import axios from "axios";
import "./blog.scss";
import MailchimpSubscribe from "react-mailchimp-subscribe";
import CustomForm from "components/organisms/Post/components/CustomForm";
const apiUrl = process.env.REACT_APP_API;
const Blog = () => {
  const [generalPost, setGeneralPost] = useState(false);
  const [allPosts, setAllPosts] = useState([]);

  useEffect(() => {
    getBlog().then((res) => {
      setAllPosts(res);
      const x = res.filter((post) => post.is_featured === 1);
      setGeneralPost(x[0]);
    });
    scrollToTop();
  }, []);

  const getBlog = async () => {
    let queryParams = {};
    queryParams.futured = 1;
    queryParams.api_key = 111;
    let res = await axios.get(`${apiUrl}/api/getblog`, {
      params: queryParams,
    });
    let { data } = res;
    return data;
  };

  return (
    <div className="mst-blogpage">
      <Helmet>
        <title>Our Blog</title>
      </Helmet>
      <FlexBox
        type="section"
        className="max1500 justify-content-center align-items-center mst-workpage__inner"
      >
        <FlexBox className="flex-column mst-workpage__content">
          <h1 className="mst-blog__title">Our Blog</h1>

          <div>
            <div>
              <div className="mst-blog__rout_path_subscribe max1500">
                <p className="mst-blog__rout_title">
                  Animation / Logo / Illustration / Character design / Video
                  animation
                </p>
                <div className="mst-blog__span">
                  <MailchimpSubscribe
                    render={({ subscribe, status, message }) => {
                      return (
                        <>
                          <CustomForm
                            status={status}
                            message={message}
                            onValidated={(formData) => subscribe(formData)}
                          />
                        </>
                      );
                    }}
                    url="https://monist.us1.list-manage.com/subscribe/post?u=0456944ecdeb7b1201c2789df&amp;id=27cbba52b5"
                  />
                </div>
              </div>
            </div>
            {generalPost && (
              <div className="mst-blog__large_post_block max1500">
                <Link className="general_post" to={`/blog/${generalPost.slug}`}>
                  <div className="mst-blog__large_post_img">
                    <img
                      src={`${apiUrl}/blog/` + generalPost.header_image}
                      alt=""
                    />
                  </div>
                  <div className="mst-blog__large_post_desc_block ">
                    <p className="mst-blog__large_post_desc_category">
                      {generalPost.category.category.name}
                    </p>
                    <h6 className="mst-blog__large_post_title">
                      {generalPost.title}
                    </h6>
                    <div
                      className="mst-blog__large_post_desc_text"
                      dangerouslySetInnerHTML={{
                        __html: generalPost.full_desc,
                      }}
                    />
                    <div className="mst-blog__large_post_user_block">
                      <div className="mst-blog__large_post_user_img">
                        <img
                          src={
                            `${apiUrl}/user/` + generalPost.author.user.avatar
                          }
                          alt=""
                        />
                      </div>
                      <div className="mst-blog__large_post_user_name">
                        <p>{generalPost.author.user.name}</p>
                        <p>{generalPost.created}</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            )}
            <div className="mst-blog__all_post_sec max1100 ">
              {allPosts.map((item, index) => {
                return (
                  <div key={index} className="mst-blog__post_block">
                    <Link to={`/blog/${item.slug}`}>
                      <div className="mst-blog__post_img_block">
                        <img
                          src={`${apiUrl}/blog/` + item.header_image}
                          alt=""
                        />
                      </div>
                      <div className="mst-blog__post_text_block">
                        <p className="mst-blog__post_category">
                          {item.category.category.name}
                        </p>
                        <p className="mst-blog__post_title">{item.title}</p>
                      </div>
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
        </FlexBox>
      </FlexBox>
    </div>
  );
};

export default Blog;

import React, { useEffect, useContext } from 'react';
import { Helmet } from "react-helmet";
import { AppContext } from "components/Context"
import { NeedAnimation } from "components/organisms/NeedAnimation/NeedAnimation";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import Tabs from "components/organisms/Tabs/Tabs";
import TabTypes from "components/organisms/Tabs/tab-types";
import LogoAndDesign from "components/organisms/LogoAndDesign/LogoAndDesign";
import Illustration from "components/organisms/Illustration/Illustration";
import Videos from "components/organisms/Videos/Videos";
import { scrollToTop } from "utils";
import "./Workpage.scss";

const Workpage = () => {

    const [selectedWorkType, setWorkType] = useContext(AppContext);
    
    useEffect(() => {
        scrollToTop();
    }, [])

    return (
        <div className="mst-workpage">
            <Helmet>
                <title>Monsto - Work</title>
                <meta
                    property="og:image"
                    content="https://i.imgur.com/3AcsSci.png"
                />
            </Helmet>
            <FlexBox type="section" className="max1500 justify-content-center align-items-center mst-workpage__inner">
                <FlexBox className="flex-column mst-workpage__content">
                    <h1 className="mst-workpage__title">Monsto's Portfolio!</h1>
                    <Tabs selectedTabName={selectedWorkType} onTabClick={setWorkType}/>
                    <div>
                        {
                            selectedWorkType === TabTypes.LOGO && (
                                <LogoAndDesign/>
                            )
                        }
                        {
                            selectedWorkType === TabTypes.ILLUSTRATION && (
                                <Illustration/>
                            )
                        }
                        {
                            selectedWorkType === TabTypes.VIDEO && (
                                <Videos/>
                            )
                        }
                    </div>
                </FlexBox>
            </FlexBox>
            <NeedAnimation/>
        </div>
    );
}

export default Workpage;

import React, { useEffect, useState } from 'react';
import Vimeo from '@u-wave/react-vimeo';
import { scrollToTop } from "utils";
import { NeedAnimation } from "components/organisms/NeedAnimation/NeedAnimation";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import "./VideoDetails.scss";
import axios from "axios";
const apiUrl = process.env.REACT_APP_API;
const VideoDetails = ({match}) => {
	let {id:videoId} = match.params
	const [state,setState] = useState({})
	useEffect(() => {
		getVideoDeitals().then(res => {
			let item = {}
			res.map((resItem) => {
				if(resItem.id == videoId) {
					item = resItem
				}

			})
			if (item) {
				setState(item)
			}
		})
		scrollToTop();
	}, [])
	const getVideoDeitals = async () => {
		let queryParams = {}
		queryParams.category = 2
		queryParams.api_key = 111
		let res = await axios.get(`${apiUrl}/api/getportfolio`, {
			params: queryParams
		});
		let {data} = res;
		return data;

	}
	let x = state

	return (
		<div className="mst-video-details">
			<FlexBox type="section" className="max1100 flex-column justify-content-center align-items-center mst-video-details__inner">

						<div>
							<h1>{x.name}</h1>
							<React.Fragment>
								<div className="w-100 content" dangerouslySetInnerHTML={{ __html: x.description }}>
								</div>
							</React.Fragment>
						</div>
				<div className="w-100 mst-video-details__vimeo" dangerouslySetInnerHTML={{ __html: x.video_embed }}>

				</div>
				<div className="w-100 content">
					{
						x.show && x.show.map((image,index)=>(
							<img key={index}
								role="presentation"
								className="mst-illustration-details__image"
								src={`${apiUrl}/portfolio/` + image}
								alt=""
							/>
						))
					}
				</div>



			</FlexBox>
			<NeedAnimation/>
		</div>
	);
}

export default VideoDetails;

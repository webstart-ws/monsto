import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import { scrollToTop } from "utils";
import "./AboutUs.scss";
import { Link } from "react-router-dom";
import axios from "axios";
const apiUrl = process.env.REACT_APP_API;

const AboutUs = () => {
  const [state, setState] = useState({
    about: [],
    members: [],
  });
  useEffect(() => {
    getAbout().then((res) => {
      setState((prevState) => {
        return {
          ...prevState,
          about: res,
        };
      });
    });
    getMembers().then((res) => {
      setState((prevState) => {
        return {
          ...prevState,
          members: res,
        };
      });
    });

    scrollToTop();
  }, []);
  const getAbout = async () => {
    let queryParams = {};
    queryParams.api_key = 111;
    let res = await axios.get(`${apiUrl}/api/getabout`, {
      params: queryParams,
    });
    let { data } = res;
    return data;
  };
  const getMembers = async () => {
    let queryParams = {};
    queryParams.api_key = 111;
    let res = await axios.get(`${apiUrl}/api/getmembers`, {
      params: queryParams,
    });
    let { data } = res;
    return data;
  };
  useEffect(() => {
    scrollToTop();
  }, []);

  return (
    <div className="mst-about-us">
      <Helmet>
        <title>Monsto - About us</title>
        <meta
            property="og:image"
            content="https://i.imgur.com/3AcsSci.png"
        />
      </Helmet>
      <FlexBox
        type="section"
        className="max1100 mst-about-us__inner flex-column justify-content-center"
      >
        <FlexBox className="flex-column justfiy-content-center align-items-center">
          {state.about.map((about, index) => {
            return (
              <div key={index}>
                <h1 className="mst-about-us__title">{about.title}</h1>
                <React.Fragment>
                  <div
                    className="w-100 content"
                    dangerouslySetInnerHTML={{ __html: about.content }}
                  ></div>
                </React.Fragment>
              </div>
            );
          })}
        </FlexBox>

        <div className="mst-about-us__members">
          {state.members.map((item, index) => {
            return (
              <div key={index} className="mst-about-us__member-item">
                <img
                  className="w-100"
                  src={`${apiUrl}/team/` + item.image}
                  alt=""
                />
                <h5>{item.name}</h5>
                <span className="profession">{item.position}</span>
                <div className="social-links">
                  <Link
                    to={{ pathname: item.dribbble }}
                    target="_blank"
                    className="social-link"
                  >
                    <svg
                      width="25"
                      height="24"
                      viewBox="0 0 25 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M12.5 0.000976562C5.60743 0.000976562 0 5.31111 0 11.8381C0 18.3652 5.60743 23.6753 12.5 23.6753C19.3926 23.6753 25 18.3652 25 11.8381C25 5.31111 19.3926 0.000976562 12.5 0.000976562ZM20.6457 5.58365C22.049 7.21943 22.913 9.28411 22.9848 11.5348C20.3854 11.0252 18.0316 11.085 15.9536 11.4708C15.5774 10.6376 15.1879 9.84377 14.7948 9.09355C17.0578 8.17036 19.0278 6.99124 20.6457 5.58365ZM19.2376 4.2284C17.6316 5.6166 15.7705 6.64444 13.8648 7.40328C12.466 4.98104 11.1277 3.15698 10.3125 2.12039C11.0184 1.97797 11.7499 1.90157 12.5 1.90157C15.0632 1.90157 17.4134 2.77762 19.2376 4.2284ZM8.28543 2.74113C8.90105 3.49655 10.3674 5.38802 11.9425 8.07913C7.98385 9.30046 4.14272 9.50623 2.29695 9.52536C3.0663 6.48188 5.31695 3.9793 8.28543 2.74113ZM2.01797 11.4308C2.03777 11.4308 2.05691 11.4309 2.07711 11.4309C3.35249 11.4309 5.68918 11.3497 8.44011 10.8747C10.0096 10.6036 11.4978 10.2388 12.8943 9.78676C13.2578 10.4729 13.6192 11.1966 13.9697 11.9552C12.0143 12.5514 10.3654 13.4092 9.0592 14.2609C6.86889 15.6892 5.33447 17.274 4.49662 18.2538C2.94534 16.5213 2.007 14.2822 2.007 11.8381C2.007 11.7015 2.01208 11.5661 2.01797 11.4308ZM5.94286 19.5874C6.66418 18.7273 8.13237 17.1618 10.2562 15.7882C11.6995 14.8546 13.1927 14.161 14.7299 13.7058C15.6218 15.895 16.3833 18.314 16.8373 20.8824C15.5142 21.4536 14.0465 21.7747 12.5 21.7747C10.0204 21.7747 7.7406 20.9543 5.94286 19.5874ZM18.6837 19.859C18.2158 17.5001 17.518 15.2821 16.7114 13.2551C18.6956 12.9381 20.7464 13.0034 22.8526 13.4515C22.3984 16.073 20.858 18.3517 18.6837 19.859Z"
                        fill="#2B96AF"
                      />
                    </svg>
                  </Link>
                  <Link
                    to={{ pathname: item.behance }}
                    target="_blank"
                    className="social-link"
                  >
                    <svg
                      width="26"
                      height="16"
                      viewBox="0 0 26 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M13.6667 11.2007C13.6667 9.21935 12.7187 7.7548 10.7602 7.20586C12.1892 6.52934 12.9344 5.50438 12.9344 3.9101C12.9344 0.766582 10.5704 0 7.84288 0H0.333374V15.783H8.05318C10.9478 15.783 13.6667 14.4074 13.6667 11.2007ZM3.83414 2.69429H7.11921C8.38212 2.69429 9.51885 3.04595 9.51885 4.49763C9.51885 5.83781 8.63449 6.3771 7.38344 6.3771H3.83414V2.69429ZM3.83306 13.1026V8.75618H7.64875C9.18991 8.75618 10.1649 9.39303 10.1649 11.0098C10.1649 12.6041 9.00117 13.1026 7.57649 13.1026H3.83306Z"
                        fill="#2B96AF"
                      ></path>
                      <path
                        d="M20.1445 15.9951C22.8568 15.9951 24.6133 14.7708 25.4587 12.1686H22.706C22.4097 13.1399 21.1885 13.6533 20.2412 13.6533C18.4135 13.6533 17.4535 12.5806 17.4535 10.7575H25.6445C25.9036 7.10924 23.8816 4 20.1435 4C16.6825 4 14.3334 6.60651 14.3334 10.021C14.3334 13.5647 16.5593 15.9951 20.1445 15.9951ZM20.0468 6.36102C21.6132 6.36102 22.4012 7.28322 22.5329 8.79141H17.4577C17.5607 7.29603 18.5516 6.36102 20.0468 6.36102Z"
                        fill="#2B96AF"
                      ></path>
                    </svg>
                  </Link>
                </div>
              </div>
            );
          })}
        </div>

        <div className="mst-about-us__welcome-image-container"></div>
      </FlexBox>
    </div>
  );
};

export default AboutUs;

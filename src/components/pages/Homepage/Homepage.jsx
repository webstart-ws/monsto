import React, { useEffect, useContext } from "react";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import { Button } from "../../atoms/Button/Button";
import { ResponsiveCarousel } from "../../organisms/ResponsiveCarousel/ResponsiveCarousel";
import { NeedAnimation } from "../../organisms/NeedAnimation/NeedAnimation";
import { AppContext } from "components/Context";
import TabTypes from "components/organisms/Tabs/tab-types";
import { scrollToTop } from "utils";
import Busuu from "assets/images/companies/busuu.png";
import Smartcom from "assets/images/companies/smartcom.png";
import CodeSignal from "assets/images/companies/codesignal.png";
import SeekVisa from "assets/images/companies/seekvisa.png";
import Thestyleoutlets from "assets/images/companies/thestyleoutlets.png";
import Neinver from "assets/images/companies/neinver.png";
import HomepageDragon from "assets/images/homepage-dragon.svg";
import WorkGroup1 from "assets/images/work-group-1.svg";
import WorkGroup2 from "assets/images/work-group-2.svg";
import WorkGroup3 from "assets/images/work-group-3.svg";
import "./Homepage.scss";
import { Helmet } from "react-helmet";

const Homepage = ({ history }) => {
  const [selectedWorkType, setWorkType] = useContext(AppContext);

  useEffect(() => {
    scrollToTop();
  }, []);

  const handleRedirect = (type) => {
    setWorkType(type);
    history.push("/work");
  };

  return (
    <div className="mst-homepage">
      <Helmet>
        <meta property="og:title" content="Monsto" />
        <meta name="fragment" content="!" />
        <meta
          property="og:image"
          content="https://i.imgur.com/3AcsSci.png"
        />
        <meta property="og:url" content="https://monsto.net" />
        <meta property="og:site_name" content="Monsto" />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="Design and animation studio" />
      </Helmet>
      <FlexBox
        type="section"
        className="max1100 mst-homepage__welcome justify-content-between"
      >
        <FlexBox className="flex-column">
          <h1 className="mst-homepage__title">
            Welcome <br /> to Monsto!
          </h1>
          <ul>
            <li>Video & Character Animation</li>
            <li>Logo Design & Animation</li>
            <li>Illustration</li>
          </ul>
          <FlexBox className="button-container mt-4">
            <Button href="/contact">Contact us</Button>
          </FlexBox>
        </FlexBox>
        <div className="mst-homepage__welcome-image-container">
          <img className="w-100" src={HomepageDragon} alt="" />
        </div>
      </FlexBox>
      <FlexBox type="section" className="mst-homepage__companies">
        <div className="max1100 mst-homepage__companies__inner desktop">
          <div className="company__logo">
            <img src={Busuu} alt="Busuu" />
          </div>
          <div className="company__logo">
            <img src={Smartcom} alt="SmartCom" />
          </div>
          <div className="company__logo">
            <img src={CodeSignal} alt="CodeSignal" />
          </div>
          <div className="company__logo">
            <img src={SeekVisa} alt="SeekVisa" />
          </div>
          <div className="company__logo">
            <img src={Thestyleoutlets} alt="TheStyleOutlets" />
          </div>
          <div className="company__logo">
            <img src={Neinver} alt="Neiver" />
          </div>
        </div>
        <div className="max1100 mst-homepage__companies__inner mobile">
          <ResponsiveCarousel slidesToSlide={2} mobileCount={2}>
            <div className="d-flex justify-content-center align-items-center company__logo--mobile">
              <img src={Busuu} alt="Busuu" />
            </div>
            <div className="d-flex justify-content-center align-items-center company__logo--mobile">
              <img src={Smartcom} alt="SmartCom" />
            </div>
            <div className="d-flex justify-content-center align-items-center company__logo--mobile">
              <img src={CodeSignal} alt="CodeSignal" />
            </div>
            <div className="d-flex justify-content-center align-items-center company__logo--mobile">
              <img src={SeekVisa} alt="SeekVisa" />
            </div>
            <div className="d-flex justify-content-center align-items-center company__logo--mobile">
              <img src={Thestyleoutlets} alt="TheStyleOutlets" />
            </div>
            <div className="d-flex justify-content-center align-items-center company__logo--mobile">
              <img src={Neinver} alt="Neiver" />
            </div>
          </ResponsiveCarousel>
        </div>
      </FlexBox>
      <FlexBox type="section" className="mst-homepage__logo-and-animation">
        <div className="d-flex justify-content-between max1100 mst-homepage__logo-and-animation__inner">
          <div className="image-container">
            <img className="w-100" src={WorkGroup1} alt="" />
          </div>
          <FlexBox className="flex-column justify-content-center">
            <h1>
              {" "}
              Logo Design <br /> & Animation{" "}
            </h1>
            <p>
              We are creating custom logos and <br />
              animations from scratch. <br />
              If you have already a logo just send <br />
              us. Review our latest projects.
            </p>
            <div
              onClick={() => handleRedirect(TabTypes.LOGO)}
              role="presentation"
              className="view-all"
            >
              <span>View all</span>
              <svg
                width="16"
                height="15"
                viewBox="0 0 16 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1 8.15477H12.865L9.232 12.5991C8.878 13.0313 8.936 13.6745 9.36 14.0344C9.785 14.3952 10.415 14.3361 10.769 13.9039L15.769 7.7878C15.808 7.73989 15.827 7.68383 15.856 7.63082C15.88 7.58801 15.909 7.55132 15.927 7.50443C15.972 7.3872 15.999 7.26386 15.999 7.1395C15.999 7.13848 16 7.13644 16 7.13542C16 7.1344 15.999 7.13236 15.999 7.13134C15.999 7.00698 15.972 6.88364 15.927 6.76642C15.909 6.71953 15.88 6.68283 15.856 6.64002C15.827 6.58701 15.808 6.53095 15.769 6.48304L10.769 0.366965C10.57 0.12538 10.286 0 10 0C9.774 0 9.547 0.0774703 9.36 0.236488C8.936 0.596317 8.878 1.23952 9.232 1.67173L12.865 6.11608H1C0.448 6.11608 0 6.57274 0 7.13542C0 7.6981 0.448 8.15477 1 8.15477Z"
                  fill="#2B96AF"
                />
              </svg>
            </div>
          </FlexBox>
        </div>
      </FlexBox>

      <FlexBox
        type="section"
        className="mst-homepage__video-and-character-animation"
      >
        <div className="d-flex justify-content-between max1100 mst-homepage__video-and-character-animation__inner">
          <div className="image-container">
            <img className="w-100" src={WorkGroup2} alt="" />
          </div>
          <FlexBox className="flex-column justify-content-center">
            <h1>
              {" "}
              Video & <br /> Character <br /> Animation{" "}
            </h1>
            <p>
              Animated explainer videos can be a <br />
              great marketing tool to help you to <br />
              deliver and promote your brand <br />
              message.
            </p>
            <div
              onClick={() => handleRedirect(TabTypes.VIDEO)}
              role="presentation"
              className="view-all"
            >
              <span>View all</span>
              <svg
                width="16"
                height="15"
                viewBox="0 0 16 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1 8.15477H12.865L9.232 12.5991C8.878 13.0313 8.936 13.6745 9.36 14.0344C9.785 14.3952 10.415 14.3361 10.769 13.9039L15.769 7.7878C15.808 7.73989 15.827 7.68383 15.856 7.63082C15.88 7.58801 15.909 7.55132 15.927 7.50443C15.972 7.3872 15.999 7.26386 15.999 7.1395C15.999 7.13848 16 7.13644 16 7.13542C16 7.1344 15.999 7.13236 15.999 7.13134C15.999 7.00698 15.972 6.88364 15.927 6.76642C15.909 6.71953 15.88 6.68283 15.856 6.64002C15.827 6.58701 15.808 6.53095 15.769 6.48304L10.769 0.366965C10.57 0.12538 10.286 0 10 0C9.774 0 9.547 0.0774703 9.36 0.236488C8.936 0.596317 8.878 1.23952 9.232 1.67173L12.865 6.11608H1C0.448 6.11608 0 6.57274 0 7.13542C0 7.6981 0.448 8.15477 1 8.15477Z"
                  fill="#2B96AF"
                />
              </svg>
            </div>
          </FlexBox>
        </div>
      </FlexBox>

      <FlexBox type="section" className="mst-homepage__illustration">
        <div className="d-flex justify-content-between max1100 mst-homepage__illustration__inner">
          <div className="image-container">
            <img className="w-100" src={WorkGroup3} alt="" />
          </div>
          <FlexBox className="flex-column justify-content-center">
            <h1> Illustration </h1>
            <p>
              We'll learn your topic and create unique <br />
              and custom illustrations for your project. <br />
              We are flexible to work with different <br />
              styles. Check our work!
            </p>
            <div
              onClick={() => handleRedirect(TabTypes.ILLUSTRATION)}
              role="presentation"
              className="view-all"
            >
              <span>View all</span>
              <svg
                width="16"
                height="15"
                viewBox="0 0 16 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1 8.15477H12.865L9.232 12.5991C8.878 13.0313 8.936 13.6745 9.36 14.0344C9.785 14.3952 10.415 14.3361 10.769 13.9039L15.769 7.7878C15.808 7.73989 15.827 7.68383 15.856 7.63082C15.88 7.58801 15.909 7.55132 15.927 7.50443C15.972 7.3872 15.999 7.26386 15.999 7.1395C15.999 7.13848 16 7.13644 16 7.13542C16 7.1344 15.999 7.13236 15.999 7.13134C15.999 7.00698 15.972 6.88364 15.927 6.76642C15.909 6.71953 15.88 6.68283 15.856 6.64002C15.827 6.58701 15.808 6.53095 15.769 6.48304L10.769 0.366965C10.57 0.12538 10.286 0 10 0C9.774 0 9.547 0.0774703 9.36 0.236488C8.936 0.596317 8.878 1.23952 9.232 1.67173L12.865 6.11608H1C0.448 6.11608 0 6.57274 0 7.13542C0 7.6981 0.448 8.15477 1 8.15477Z"
                  fill="#2B96AF"
                />
              </svg>
            </div>
          </FlexBox>
        </div>
      </FlexBox>
      <div className="mst-homepage__need-animation__section">
        <NeedAnimation />
      </div>
    </div>
  );
};

export default Homepage;

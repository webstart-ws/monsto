import React, { Component } from "react";
import { Route } from "react-router";
import Header from "../../organisms/Header/Header";
import Footer from "../../organisms/Footer/Footer";
import { withRouter } from "react-router-dom";
class GuestLayout extends Component {

    render() {
        const { component: ChildComponent, location, ...rest } = this.props;
        return (
            <div>
                <Header />
                <div>
                    <Route
                        {...rest}
                        render={matchProps => {
                            return <ChildComponent {...matchProps} />;
                        }}
                    />
                </div>
                <Footer />
            </div>
        );
    }
}

export default withRouter(GuestLayout);

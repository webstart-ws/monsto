import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import "./Illustration.scss";
import {scrollToTop} from "../../../utils";
import axios from "axios";
const apiUrl = process.env.REACT_APP_API;
const Illustration = () => {
    const [state,setState] = useState([])
    useEffect(() => {
        getIlustration().then(res => {
            setState(res)
        })
        scrollToTop();
    }, [])
    const getIlustration = async () => {
        let queryParams = {}
        queryParams.category = 3
        queryParams.api_key = 111
        let res = await axios.get(`${apiUrl}/api/getportfolio`, {
            params: queryParams
        });
        let {data} = res;
        return data;
    }
    return (
        <FlexBox className="max1200 mst-illustration justify-content-center">
            <Helmet>
                <title>Monsto - Our work / Illlustration</title>
            </Helmet>
            <div className="mst-illustration__inner">
                {
                    state.map((illustration,index)=>(
                        <Link key={index} to={`work/illustration/${illustration.id}`}>
                            <div className="mst-illustration__item">
                                <img className="mst-illustration__image" src={`${apiUrl}/portfolio/` + illustration.header_image} alt="" />
                            </div>
                        </Link>
                    ))
                }
            </div>
        </FlexBox>
    );
}

export default Illustration;

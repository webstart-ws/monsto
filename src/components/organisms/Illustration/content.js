import CGClassroomThumbnail from "assets/images/illustration/cg-classroom-stickers/CG Classroom stickers cover.svg";
import CGClassroom1 from "assets/images/illustration/cg-classroom-stickers/CG Classroom stickers 1.gif";
import CGClassroom2 from "assets/images/illustration/cg-classroom-stickers/CG Classroom stickers 2.gif";
import CGClassroom3 from "assets/images/illustration/cg-classroom-stickers/CG Classroom stickers 3.gif";
import CGClassroom4 from "assets/images/illustration/cg-classroom-stickers/CG Classroom stickers 4.svg";
import CGClassroom5 from "assets/images/illustration/cg-classroom-stickers/CG Classroom stickers 5.gif";

import NursulaThumbnail from "assets/images/illustration/nursula/Nursula Cover.svg";
import Nursula1 from "assets/images/illustration/nursula/Nursula 1.svg";
import Nursula2 from "assets/images/illustration/nursula/Nursula 2.svg";
import Nursula3 from "assets/images/illustration/nursula/Nursula 3.svg";

import WinterMoodThumbnail from "assets/images/illustration/winter-mood/Winter Mood cover.svg";
import WinterMood1 from "assets/images/illustration/winter-mood/Winter Mood 1.svg";
import WinterMood2 from "assets/images/illustration/winter-mood/Winter Mood 2.svg";
import WinterMood3 from "assets/images/illustration/winter-mood/Winter Mood 3.svg";
import WinterMood4 from "assets/images/illustration/winter-mood/Winter Mood 4.svg";
import WinterMood5 from "assets/images/illustration/winter-mood/Winter Mood 5.svg";
import WinterMood6 from "assets/images/illustration/winter-mood/Winter Mood 6.svg";
import WinterMood7 from "assets/images/illustration/winter-mood/Winter Mood 7.svg";

import TrashyThumbnail from "assets/images/illustration/trashy/Trashy cover.svg";
import Trashy1 from "assets/images/illustration/trashy/Trashy 1.svg";
import Trashy2 from "assets/images/illustration/trashy/Trashy 2.svg";

import BorderlessThumbnail from "assets/images/illustration/borderless-360/Borderless 360 cover.svg";
import Borderless1 from "assets/images/illustration/borderless-360/Borderless 360_1.svg";
import Borderless2 from "assets/images/illustration/borderless-360/Borderless 360_2.svg";
import Borderless3 from "assets/images/illustration/borderless-360/Borderless 360_3.svg";
import Borderless4 from "assets/images/illustration/borderless-360/Borderless 360_4.svg";
import Borderless5 from "assets/images/illustration/borderless-360/Borderless 360_5.svg";
import Borderless6 from "assets/images/illustration/borderless-360/Borderless 360_6.svg";
import Borderless7 from "assets/images/illustration/borderless-360/Borderless 360_7.svg";

const Content = [
  {
    thumbnail: CGClassroomThumbnail,
    id: 1,
    images: [
      CGClassroom1,
      CGClassroom2,
      CGClassroom3,
      CGClassroom4,
      CGClassroom5,
    ],
    title: 'CG Classrom',
    text: 'CG classroom is a place to learn computer graphics and design.'
  },
  {
    thumbnail: NursulaThumbnail,
    id: 2,
    images: [
      Nursula1,
      Nursula2,
      Nursula3,
    ],
    title: 'Nursula',
    text: 'Character design for Nursula - free baby breastfeeding & bottle tracker app.'
  },
  {
    thumbnail: WinterMoodThumbnail,
    id: 3,
    images: [
      WinterMood1,
      WinterMood2,
      WinterMood3,
      WinterMood4,
      WinterMood5,
      WinterMood6,
      WinterMood7
    ],
    title: 'Winter Mood',
    text: ''
  },
  {
    thumbnail: TrashyThumbnail,
    id: 4,
    images: [
      Trashy1,
      Trashy2,
    ],
    title: 'Trashy',
    text: ''
  },
  {
    thumbnail: BorderlessThumbnail,
    id: 5,
    images: [
      Borderless1,
      Borderless2,
      Borderless3,
      Borderless4,
      Borderless5,
      Borderless6,
      Borderless7,
    ],
    title: 'Borderless 360',
    text: ''
  },
];

export default Content;
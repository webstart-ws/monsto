import React from "react";
import { Alert } from "react-bootstrap";
import "../Postpage.scss";


const CustomForm = ({ status, message, onValidated }) => {
  let email;
  const submit = () =>
    email &&
    email.value.indexOf("@") > -1 &&
    onValidated({
      EMAIL: email.value,
    });

  return (
    <>
      {status === "sending" && (
        <Alert
          className="mailchimp_submit"
          variant="primary"
        >
          Sending...
        </Alert>
      )}
      {status === "error" && (
        <Alert
          className="mailchimp_submit"
          variant={"danger"}
        >
          <div dangerouslySetInnerHTML={{ __html: message }}></div>
        </Alert>
      )}
      {status === "success" && (
        <Alert
          className="mailchimp_submit"
          variant={"success"}
        >
          {message}
        </Alert>
      )}
      <div>
        <input
          ref={(node) => (email = node)}
          type="email"
          placeholder="Your email"
        />
        <button onClick={submit}>Subscribe</button>
      </div>
    </>
  );
};

export default CustomForm;

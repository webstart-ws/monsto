import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import { scrollToTop } from "utils";
import axios from "axios";
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterIcon,
  FacebookIcon,
} from "react-share";
import "./Postpage.scss";
import MailchimpSubscribe from "react-mailchimp-subscribe";
import CustomForm from "./components/CustomForm";
const apiUrl = process.env.REACT_APP_API;

const Post = (props) => {
  const [state, setState] = useState({
    snglPost: {},
    samePosts: [],
  });
  let { id: blogSlug } = props.match.params;
  let { category: blogCategory } = props.match.params;
  useEffect(() => {
    scrollToTop();
  }, []);
  useEffect(() => {
    getPostPage().then((res) => {
      setState((prevState) => {
        return {
          ...prevState,
          snglPost: {
            ...res,
            full_desc: res.full_desc.replace(/<p>&nbsp;<\/p>/gi, ""),
          },
        };
      });
    });

    getSamePosts().then((res) => {
      setState((prevState) => {
        return {
          ...prevState,
          samePosts: res,
        };
      });
    });
    scrollToTop();
  }, [blogSlug]);
  const getPostPage = async () => {
    let queryParams = {};
    queryParams.api_key = 111;
    queryParams.slug = blogSlug;
    let res = await axios.get(`${apiUrl}/api/getsnglepost`, {
      params: queryParams,
    });
    let { data } = res;
    return data;
  };
  const getSamePosts = async () => {
    let queryParams = {};
    queryParams.api_key = 111;
    queryParams.category = blogCategory;
    let res = await axios.get(`${apiUrl}/api/getblogwithcoategory`, {
      params: queryParams,
    });
    let { data } = res;
    return data;
  };
  let img = `${apiUrl}/blog/` + state.snglPost.header_image;
  return (
    <div className="mst-postpage">
      <Helmet>
        <meta name="fragment" content="!" />
        <title>{state.snglPost.title}</title>
        <meta property="og:title" content={state.snglPost.title} />
        <meta property="og:image" content={img} />
        <meta
          property="og:url"
          content={"https://monsto.net/blog/" + blogSlug}
        />
        <meta property="og:site_name" content="MONSTO" />
        <meta name="description" content={state.snglPost.full_desc} />
      </Helmet>
      <FlexBox
        type="section"
        className="max1500 justify-content-center align-items-center mst-workpage__inner"
      >
        <FlexBox className="flex-column mst-workpage__content">
          <div>
            <div>
              <div className="mst-post__rout_path_subscribe max1500">
                <p className="mst-post__rout_title">{state.snglPost.title}</p>
                <div className="mst-blog__span">
                  <MailchimpSubscribe
                    render={({ subscribe, status, message }) => {
                      return (
                        <>
                          <CustomForm
                            status={status}
                            message={message}
                            onValidated={(formData) => subscribe(formData)}
                          />
                        </>
                      );
                    }}
                    url="https://monist.us1.list-manage.com/subscribe/post?u=0456944ecdeb7b1201c2789df&amp;id=27cbba52b5"
                  />
                </div>
              </div>
            </div>
            <div>
              <div className="mst-post__large_post_block max1500">
                <div className="mst-post__large_post_img w-100">
                  <img
                    src={
                      state.snglPost.header_image &&
                      `${apiUrl}/blog/${state.snglPost.header_image}`
                    }
                    alt=""
                  />
                </div>
              </div>
              <div className="mst-post__all_post_sec max1100 ">
                <div
                  className="mst-post__post-article"
                  dangerouslySetInnerHTML={{
                    __html: state.snglPost.full_desc,
                  }}
                ></div>
              </div>
              <div className="mst-post__social-block-subscribe max1100">
                <div className="social-block">
                  <span className="share-title">Share</span>
                  <LinkedinShareButton url={window.location.href}>
                    <LinkedinIcon size={32} round />
                  </LinkedinShareButton>
                  <TwitterShareButton url={window.location.href}>
                    <TwitterIcon size={32} round />
                  </TwitterShareButton>
                  <FacebookShareButton url={window.location.href}>
                    <FacebookIcon size={32} round />
                  </FacebookShareButton>
                </div>
                <div className="subscribe_block">
                  <div className="mst-blog__span">
                    <MailchimpSubscribe
                      render={({ subscribe, status, message }) => {
                        return (
                          <>
                            <CustomForm
                              status={status}
                              message={message}
                              onValidated={(formData) => subscribe(formData)}
                            />
                          </>
                        );
                      }}
                      url="https://monist.us1.list-manage.com/subscribe/post?u=0456944ecdeb7b1201c2789df&amp;id=27cbba52b5"
                    />
                  </div>
                </div>
              </div>
              <div className="mst-post__same-posts-block max1100">
                {state.samePosts.map((item, index) => {
                  return (
                    <div className="mst-blog__post_block" key={index}>
                      <Link to={`/blog/${item.slug}`}>
                        <div className="mst-blog__post_img_block">
                          <img
                            src={`${apiUrl}/blog/` + item.header_image}
                            alt=""
                          />
                        </div>
                        <div className="mst-blog__post_text_block">
                          <p className="mst-blog__post_category">
                            {item.category.category.name}
                          </p>
                          <p className="mst-blog__post_title">{item.title}</p>
                        </div>
                      </Link>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </FlexBox>
      </FlexBox>
    </div>
  );
};

export default Post;

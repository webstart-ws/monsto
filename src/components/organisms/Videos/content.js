import EquityThumbnail from "assets/images/video/equity/tmb1.svg";
import EquityImg1 from "assets/images/video/equity/image1.svg";
import EquityImg2 from "assets/images/video/equity/image2.svg";
import EquityImg3 from "assets/images/video/equity/mius.svg";
import EquityImg4 from "assets/images/video/equity/GIF_01.gif";
import EquityImg5 from "assets/images/video/equity/GIF_02.gif";

const Content = [
  {
    thumbnail: EquityThumbnail,
    id: 1,
    images: [
      EquityImg4,
      EquityImg5,
      EquityImg1,
      EquityImg2,
      EquityImg3
    ],
    title: 'Equity',
    text: 'The Equity.no have developed an automated system that follow invoices after they have been sent to the client. <br/> <br/> The system basically does everything to make sure the invoice is payed as quickly as possible. That includes reminders and debt collection. <br/> <br/> The animated explainer video describes the sequence of events when Equity follows an invoice.'
  }
];

export default Content;
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";
import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import PlayIcon from "assets/images/video/play-icon.svg";
import "./Videos.scss";
import {scrollToTop} from "../../../utils";
import axios from "axios";
const apiUrl = process.env.REACT_APP_API;

const Videos = (props) => {
    const [state,setState] = useState([])
    useEffect(() => {
        getVide().then(res => {
            setState(res)
        })
        scrollToTop();
    }, [])
    const getVide = async () => {
        let queryParams = {}
        queryParams.category = 2
        queryParams.api_key = 111
        let res = await axios.get(`${apiUrl}/api/getportfolio`, {
            params: queryParams
        });
        let {data} = res;
        return data;

    }
    return (
        <FlexBox className="max1200 mst-video justify-content-center">
            <Helmet>
                <title>Monsto - Our work / Illlustration</title>
            </Helmet>
            <div className="mst-video__inner">
                {
                    state.map((video,index)=>(
                        <Link key={index} to={`work/video/${video.id}`}>
                            <div className="mst-video__item">
                                <img className="mst-video__image" src={`${apiUrl}/portfolio/` + video.header_image} alt="" />
                                <div className="mst-video__play-icon">
                                    <img src={PlayIcon}></img>
                                </div>
                            </div>
                        </Link>
                    ))
                }
            </div>
        </FlexBox>
    );
}

export default Videos;

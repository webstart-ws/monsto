import React from 'react';
import { Button } from "../../atoms/Button/Button";
import { FlexBox } from '../../atoms/FlexBox/FlexBox';

import Dragon from "assets/images/Dragon.svg";
import "./NeedAnimation.scss";

export const NeedAnimation = () => {
    return (
        <div className="mst-need-animation">
            <div className="max1100 mst-need-animation__inner">
                <div className="d-flex align-items-center mst-need-animation__content position-relative">
                    <img className="position-absolute dragon-image" src={Dragon} alt="" />
                    <div className="d-flex w-100 justify-content-between align-items-center mst-need-animation__content-inner">
                        <FlexBox>
                            <h1>Need a cute animation?</h1>
                        </FlexBox>
                        <FlexBox className="button-container">
                            <Button href="/contact">Contact us</Button>
                        </FlexBox>
                    </div>
                </div>
            </div>
        </div>
    );
};
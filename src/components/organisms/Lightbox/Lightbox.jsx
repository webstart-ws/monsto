import React, {useEffect, useState} from 'react';

import { FlexBox } from "../../atoms/FlexBox/FlexBox";
import CloseIcon from "assets/images/Close.svg";

import "./Lightbox.scss";

const Lightbox = ({
    src,
    title,
    onClose
}) => {

    const [showLightbox, setShowLightbox] = useState(false);
    useEffect(()=>{
        if(src) {
            document.body.style.overflow = 'hidden';
        }
    },[src]);

    const handleClose = () => {
        document.body.style.overflow = 'initial';
        setShowLightbox(false);
        onClose();
    }

    const handleShow = () => {
        setTimeout(() => {
            setShowLightbox(true)
        }, 100);
    }
    return (
        <div onClick={handleClose} className={`mst-lightbox ${showLightbox ? 'active' : 'inactive'}`}>
					<div onClick={(e)=>{
                        e.stopPropagation();
                        e.nativeEvent.stopImmediatePropagation();
                    }} className="mst-lightbox__inner">
						<div className="mst-lightbox__header">
								<h2>{title}</h2>
								<div
										role="presentation"
										onClick={handleClose}
										className="mst-lightbox__close">
										<img src={CloseIcon} alt=""/>
								</div>
						</div>
						<img className="mst-lightbox__image" onLoad={handleShow} src={src} alt="" />
					</div>
        </div>
    );
}

export default Lightbox;
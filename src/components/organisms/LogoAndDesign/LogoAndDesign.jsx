import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";

import Lightbox from "components/organisms/Lightbox/Lightbox";

import { FlexBox } from "../../atoms/FlexBox/FlexBox";

import Images from "./images";

import "./LogoAndDesign.scss";
import {scrollToTop} from "../../../utils";
import axios from "axios";
const apiUrl = process.env.REACT_APP_API;

const LogoAndDesign = () => {
	const [state,setState] = useState([])
	useEffect(() => {
		getLogoAndDesign().then(res => {
			setState(res)
		})
		scrollToTop();
	}, [])
	const getLogoAndDesign = async () => {
		let queryParams = {}
		queryParams.category = 1
		queryParams.api_key = 111
		let res = await axios.get(`${apiUrl}/api/getportfolio`, {
			params: queryParams
		});
		let {data} = res;
		return data;

	}
	const [lightBoxSettings, setLightboxSettings] = useState({
		isOpened: false,
		img: '',
		title: ''
	});

	return (
		<FlexBox className="max1200 mst-logo-and-design justify-content-center">
			<Helmet>
				<title>Monsto - Our work / Logo and Design</title>
			</Helmet>
			<div className="mst-logo-and-design__inner">
				{
					state.map((image,index)=>(
						<div
							key={index}
							role="presentation"
							className="mst-logo-and-design__item"
							onClick={() => setLightboxSettings({
								isOpened: true,
								img:`${apiUrl}/portfolio/` + image.show,
								title: image.name
							})}>
							<img className="mst-logo-and-design__image" src={`${apiUrl}/portfolio/` + image.header_image} alt=""/>
						</div>
					))
				}
			</div>
			{lightBoxSettings.isOpened && (
				<Lightbox
					title = {lightBoxSettings.title}
					src={lightBoxSettings.img}
					onClose={() => {
						setLightboxSettings({
							isOpened: false,
							img: '',
							title: ''
						})
					}}
				/>
			)}
		</FlexBox>
	);
}

export default LogoAndDesign;

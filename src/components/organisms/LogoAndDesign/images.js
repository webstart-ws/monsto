import TrackWell from "assets/images/logo-and-design/track-well/track-well_SVG.svg";
import TrackWellGIF from "assets/images/logo-and-design/track-well/track-well_GIF.gif";
import StudioBou from "assets/images/logo-and-design/studio-bou/studio-bou_SVG.svg";
import StudioBouGIF from "assets/images/logo-and-design/studio-bou/studio-bou_GIF.gif";
import Squizify from "assets/images/logo-and-design/squizify/squizify_SVG.svg";
import SquizifyGIF from "assets/images/logo-and-design/squizify/squizify_GIF.gif";
import SolvingIt from "assets/images/logo-and-design/solving-it/solving-it_SVG.svg";
import SolvingItGIF from "assets/images/logo-and-design/solving-it/solving-it_GIF.gif";
import SeekVisa from "assets/images/logo-and-design/seek-visa/seek-visa_SVG.svg";
import SeekVisaGIF from "assets/images/logo-and-design/seek-visa/seek-visa_GIF.gif";
import RampageRumble from "assets/images/logo-and-design/rampage-rumble/rampage-rumble_SVG.svg";
import RampageRumbleGIF from "assets/images/logo-and-design/rampage-rumble/rampage-rumble_GIF.gif";
import Neinver from "assets/images/logo-and-design/neinver/neinver_SVG.svg";
import NeinverGIF from "assets/images/logo-and-design/neinver/neinver_GIF.gif";
import NaveLawFirm from "assets/images/logo-and-design/nave-law-firm/nave-law-firm_SVG.svg";
import NaveLawFirmGIF from "assets/images/logo-and-design/nave-law-firm/nave-law-firm_GIF.gif";
import NanoSocks from "assets/images/logo-and-design/nano-socks/nano-socks_SVG.svg";
import NanoSocksGIF from "assets/images/logo-and-design/nano-socks/nano-socks_GIF.gif";
import Naawel from "assets/images/logo-and-design/naawel/naawel_SVG.svg";
import NaawelGIF from "assets/images/logo-and-design/naawel/naawel_GIF.gif";
import Mobiclocks from "assets/images/logo-and-design/mobiclocks/mobiclocks_SVG.svg";
import MobiclocksGIF from "assets/images/logo-and-design/mobiclocks/mobiclocks_GIF.gif";
import LabelRader from "assets/images/logo-and-design/label-radar/label-radar_SVG.svg";
import LabelRaderGIF from "assets/images/logo-and-design/label-radar/label-radar_GIF.gif";
import Kemp from "assets/images/logo-and-design/kemp/kemp_SVG.svg";
import KempGIF from "assets/images/logo-and-design/kemp/kemp_GIF.gif";
import Ecosio from "assets/images/logo-and-design/ecosio/ecosio_SVG.svg";
import EcosioGIF from "assets/images/logo-and-design/ecosio/ecosio_GIF.gif";
import DesTalks from "assets/images/logo-and-design/des-talks/des-talks_SVG.svg";
import DesTalksGIF from "assets/images/logo-and-design/des-talks/des-talks_GIF.gif";
import Biztop from "assets/images/logo-and-design/biztop/biztop_SVG.svg";
import BiztopGIF from "assets/images/logo-and-design/biztop/biztop_GIF.gif";
import Apeel from "assets/images/logo-and-design/apeel/apeel_SVG.svg";
import ApeelGIF from "assets/images/logo-and-design/apeel/apeel_GIF.gif";
import CodeSignal from "assets/images/logo-and-design/code-signal/code-signal_SVG.svg";
import CodeSignalGIF from "assets/images/logo-and-design/code-signal/code-signal_GIF.gif";
import TheStyleOutlets from "assets/images/logo-and-design/the-style-outlets/TSO_SVG.svg";
import TheStyleOutletsGIF from "assets/images/logo-and-design/the-style-outlets/TSO_GIF.gif";
import Copilot from "assets/images/logo-and-design/copilot/copilot.jpg";
import CopilotGIF from "assets/images/logo-and-design/copilot/copilot_GIF.gif";
import Aberrant from "assets/images/logo-and-design/aberrant/aberrant_SVG.svg";
import AberrantGIF from "assets/images/logo-and-design/aberrant/aberrant_GIF.gif";


const Images = [
  {
    img: Copilot,
    gif: CopilotGIF,
    id: 1,
    name: 'Copilot'
  },
  {
    img: TheStyleOutlets,
    gif: TheStyleOutletsGIF,
    id: 2,
    name: 'The Style Outlets'
  },
  {
    img: CodeSignal,
    gif: CodeSignalGIF,
    id: 3,
    name: 'CodeSignal'
  },
  {
    img: Apeel,
    gif: ApeelGIF,
    id: 4,
    name: 'Apeel'
  },
  {
    img: DesTalks,
    gif: DesTalksGIF,
    id: 5,
    name: 'Design Talks'
  },
  {
    img: Ecosio,
    gif: EcosioGIF,
    id: 6,
    name: 'Ecosio'
  },
  {
    img: Kemp,
    gif: KempGIF,
    id: 7,
    name: 'Kemp'
  },
  {
    img: LabelRader,
    gif: LabelRaderGIF,
    id: 8,
    name: 'LabelRadar'
  },
  {
    img: Mobiclocks,
    gif: MobiclocksGIF,
    id: 9,
    name: 'mobiclocksc'
  },
  {
    img: Naawel,
    gif: NaawelGIF,
    id: 10,
    name: 'Naawel'
  },
  {
    img: NanoSocks,
    gif: NanoSocksGIF,
    id: 11,
    name: 'NANOSOCKS'
  },
  {
    img: NaveLawFirm,
    gif: NaveLawFirmGIF,
    id: 12,
    name: 'NEW LAW FIRM'
  },
  {
    img: Neinver,
    gif: NeinverGIF,
    id: 13,
    name: 'NEINVER'
  },
  {
    img: RampageRumble,
    gif: RampageRumbleGIF,
    id: 14,
    name: 'RAMPAGE RUMBLE'
  },
  {
    img: SeekVisa,
    gif: SeekVisaGIF,
    id: 15,
    name: 'seekvisa'
  },
  {
    img: SolvingIt,
    gif: SolvingItGIF,
    id: 16,
    name: 'SOLVING IT'
  },
  {
    img: Aberrant,
    gif: AberrantGIF,
    id: 17,
    name: 'abberant'
  },
  {
    img: Squizify,
    gif: SquizifyGIF,
    id: 18,
    name: 'Squizify'
  },
  {
    img: StudioBou,
    gif: StudioBouGIF,
    id: 19,
    name: 'Studia Bou'
  },
  {
    img: TrackWell,
    gif: TrackWellGIF,
    id: 20,
    name: 'Trackwell'
  },
  {
    img: Biztop,
    gif: BiztopGIF,
    id: 21,
    name: 'biztop'
  },
];

export default Images;
import React from 'react';
import Carousel from 'react-multi-carousel';
import "./ResponsiveCarousel.scss";
import 'react-multi-carousel/lib/styles.css';

export const ResponsiveCarousel = ({
  children,
  arrows = false,
  superLargeDesktopCount = 5,
  desktopCount = 5,
  tabletCount = 4,
  smallTabletCount = 3,
  mobileCount = 1,
  showDots = true,
  className = '',
  slidesToSlide = 1,
}) => {

  const responsiveCounts = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: superLargeDesktopCount || 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: desktopCount || 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 768 },
      items: tabletCount || 4,
    },
    smallTablet: {
      breakpoint: { max: 768, min: 464 },
      items: smallTabletCount || 3,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: mobileCount || 1,
    },
  };

  return (
    <div className={`w-100 h-100 ${className}`}>
      <Carousel
        arrows={arrows}
        slidesToSlide={slidesToSlide}
        showDots={showDots}
        itemClass="mst__responsive-carousel"
        responsive={responsiveCounts}
      >
        {children}
      </Carousel>
    </div>
  );
};

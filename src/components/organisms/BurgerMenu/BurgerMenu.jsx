import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router-dom";
import { Link } from 'react-router-dom';

import { FlexBox } from "../../atoms/FlexBox/FlexBox";

import BurgerMenuIcon from "assets/images/burger-menu.svg";
import "./BurgerMenu.scss";

const BurgerMenu = ({history}) => {

    const [isOpened, setOpened ] = useState(false);

    const handleMenuOpen = () => {
        document.body.style.overflow = !isOpened ? 'hidden' : 'initial';
        setOpened(!isOpened)
    }

    useEffect(()=>{
        document.body.style.overflow = 'initial';
        setOpened(false);
    },[history.location.pathname])

    return (
       <div className="mst-burger-menu">
            <div onClick={handleMenuOpen}>
                <img src={BurgerMenuIcon} alt=""/>
           </div>
            <div className={`mst-burger-menu__inner ${isOpened ? 'active' : 'inactive'}`}>
                <ul className="mst-burger-menu__navigation">
                    <li className="mst-burger-menu__navigation-item">
                        <Link to="/work"> Work </Link>
                    </li>
                     <li className="mst-burger-menu__navigation-item">
                            <Link to="/blog"> Blog </Link>
                        </li>
                    <li className="mst-burger-menu__navigation-item">
                        <Link to="/about"> About </Link>
                    </li>
                    <li className="mst-burger-menu__navigation-item">
                        <Link to="/contact"> Contact </Link>
                    </li>
                </ul>              
           </div>
       </div>
    );
}

export default withRouter(BurgerMenu);
import React from 'react';
import LogoDesign from "assets/images/tabs/design.svg";
import Illustration from "assets/images/tabs/illustration.svg";
import Video from "assets/images/tabs/video.svg";
import TabTypes from "./tab-types";
import "./Tabs.scss";


export const tabs = [
    {
        name: TabTypes.LOGO,
        text: 'Logo Design & Animation',
        src: LogoDesign
    },
    {
        name: TabTypes.VIDEO,
        text: 'Video & Character Animation',
        src: Video
    },
    {
        name: TabTypes.ILLUSTRATION,
        text: 'Illustration',
        src: Illustration
    }
]

const Tabs = ({
                  selectedTabName,
                  onTabClick = () => {
                  }
              }) => {
    return (
        <div className="mst-tabs">
            <div className="max1100 mst-tabs__inner">
                {
                    tabs.map((tab, index) => (
                        <div key={index} className="tabs_images_block">

                            <div className="mst-tabs__images">
                                <div key={index} role="presentation" onClick={() => onTabClick(tab.name)}
                                     className={`mst-tabs__tab-item ${selectedTabName === tab.name ? 'active' : 'inactive'}`}>
                                    <div className="mst-tabs__tab-item__inner" key={tab.name}>
                                        <img src={tab.src} alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div className="mst-tabs__divider"/>
                            <div className="mst-tabs__texts">
                                <div key={index} role="presentation" onClick={() => onTabClick(tab.name)}
                                     className={`mst-tabs__tab-item__text  ${selectedTabName === tab.name ? 'active' : 'inactive'}`}>
                                    <span>{tab.text}</span>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    );
};

export default Tabs;

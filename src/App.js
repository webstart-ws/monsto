import React, {useRef, useState} from 'react';
import Routes from "./routes";
import {AppContext} from "components/Context";
import "./App.scss";
const {Provider} = AppContext;

function App() {
    const [selectedWorkType, setWorkType] = useState('logo');

    return (
        <div  className="App">
            <Provider value={[selectedWorkType, setWorkType]}>
                <Routes/>
            </Provider>
        </div>

    );
}

export default App;

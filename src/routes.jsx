import React, { lazy, Suspense, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Switch, Route } from "react-router";
import Homepage from "components/pages/Homepage/Homepage";
import Workpage from "components/pages/Workpage/Workpage";
import AboutUs from "components/pages/AboutUs/AboutUs";
import Contact from "components/pages/Contact/Contact";
import IllustrationDetails from "components/pages/IllustrationDetails/IllustrationDetails";
import VideoDetails from "components/pages/VideoDetails/VideoDetails";
import Blog from "components/pages/Blog/Blog";
import GuestLayout from "components/layouts/GuestLayout/GuestLayout";
import routeCodes from "route-codes";
import Post from "./components/organisms/Post/Postpage";

const Routes = () => {

    return (
        <Suspense fallback={<div>loading</div>}>
            <Router>
                <Switch>
                    <GuestLayout
                        exact
                        path={routeCodes.HOMEPAGE}
                        component={Homepage}
                    />
                    <GuestLayout
                        exact
                        path={routeCodes.WORK}
                        component={Workpage}
                    />
                    <GuestLayout
                        exact
                        path={routeCodes.ABOUT_US}
                        component={AboutUs}
                    />
                    <GuestLayout
                        exact
                        path={routeCodes.CONTACT}
                        component={Contact}
                    />
                    <GuestLayout
                        exact
                        path={routeCodes.Blog}
                        component={Blog}
                    />
                    <GuestLayout
                        path={routeCodes.ILLUSTRATION_DETAILS}
                        component={IllustrationDetails}
                    />
                    <GuestLayout
                        path={routeCodes.VIDEO_DETAILS}
                        component={VideoDetails}
                    />
                    <GuestLayout
                        path={routeCodes.Post}
                        component={Post}
                    />

                </Switch>
            </Router>
        </Suspense>
    )
}

export default Routes;